variable "emsyte_account_id" {
  default = "592292237331"
}

variable "analytics_account_id" {
  default = "282130985961"
}

variable "justwebinar_account_id" {
  default = "392833452340"
}

variable "tailoredlabs_account_id" {
  default = "739004611138"
}

variable "aws_default_region" {
  default = "us-east-1"
}

variable "tf_state_bucket" {
  default = "tf-infra-state"
}


