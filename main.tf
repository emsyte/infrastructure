// Configure AWS provider
provider "aws" {
  region = var.aws_default_region
}

provider "aws" {
  alias               = "emsyte"
  profile             = "emsyte"
  region              = var.aws_default_region
  allowed_account_ids = [var.emsyte_account_id]
}

provider "aws" {
  alias               = "emsyte-analytics"
  profile             = "emsyteAnalytics"
  region              = var.aws_default_region
  allowed_account_ids = [var.analytics_account_id]
}

provider "aws" {
  alias               = "just-webinar"
  profile             = "justWebinar"
  region              = var.aws_default_region
  allowed_account_ids = [var.justwebinar_account_id]
}

provider "aws" {
  alias               = "tailored-labs"
  profile             = "tailoredLabs"
  region              = var.aws_default_region
  allowed_account_ids = [var.tailoredlabs_account_id]
}

terraform {
  backend "s3" {
    encrypt = true
    bucket  = "emsyte-tf-infra-state"
    region  = "us-east-1"
    key     = "terraform.tfstate"
    profile = "emsyte"
  }
}

// Set human readable alias for the account
resource "aws_iam_account_alias" "emsyte" {
  account_alias = "emsyte"
  provider      = aws.emsyte
}

resource "aws_iam_account_alias" "emsyte-analytics" {
  account_alias = "emsyte-analytics"
  provider      = aws.emsyte-analytics
}

resource "aws_iam_account_alias" "just-webinar" {
  account_alias = "just-webinar"
  provider      = aws.just-webinar
}

resource "aws_iam_account_alias" "tailored-labs" {
  account_alias = "tailored-labs"
  provider      = aws.tailored-labs
}


// GSuite IDP SAML Module
module "gsuite_sso_emsyte" {
  source             = "./modules/sso"
  idp_data_file_path = "files/GoogleIDPMetadata.xml"

  providers = {
    aws = aws.emsyte
  }
}

module "gsuite_sso_emsyte_analytics" {
  source             = "./modules/sso"
  idp_data_file_path = "files/GoogleIDPMetadata.xml"

  providers = {
    aws = aws.emsyte-analytics
  }
}

module "gsuite_sso_just_webinar" {
  source             = "./modules/sso"
  idp_data_file_path = "files/GoogleIDPMetadata.xml"

  providers = {
    aws = aws.just-webinar
  }
}

module "gsuite_sso_tailored_labs" {
  source             = "./modules/sso"
  idp_data_file_path = "files/GoogleIDPMetadata.xml"

  providers = {
    aws = aws.tailored-labs
  }
}


# module "idp-gsuite" {
#   source   = "DNXLabs/idp-gsuite/aws"
#   version  = "1.3.0"
#   org_name = "emsyte"
#   metadata = file("./files/GoogleIDPMetadata.xml")
#   # insert the 2 required variables here
# }
